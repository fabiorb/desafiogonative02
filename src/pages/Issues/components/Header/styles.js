import { StyleSheet } from 'react-native';
import { general, metrics } from 'styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default styles;
